package input;

import base.BaseTests;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;


public class InputTests extends BaseTests {

    @Test
    public void testInput(){
        var page = homePage.clickInputsLink();
        page.inputNumber("123");
    }
}

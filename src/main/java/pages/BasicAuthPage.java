package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class BasicAuthPage {

    private WebDriver driver;

    public BasicAuthPage(WebDriver driver){
        this.driver = driver;
    }

    private By successMessage = By.cssSelector(".example p");

    public void successfulLoginViaUrl(){
        driver.get("http://admin:admin@the-internet.herokuapp.com/basic_auth");
    }

    public String getSuccessfulLoginText(){
        return driver.findElement(successMessage).getText();
    }
}

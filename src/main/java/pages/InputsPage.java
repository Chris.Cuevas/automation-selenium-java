package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InputsPage {

    private WebDriver driver;
    private By inputField = By.cssSelector("input[type='number']");

    public InputsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void inputNumber(String input){
        driver.findElement(inputField).sendKeys(input);
    }

    public String getInputText(){
        return driver.findElement(inputField).getText();
    }


}
